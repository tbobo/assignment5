#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    return render_template('showBook.html', books=books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    if request.method == 'GET':
        return render_template('newBook.html')
    else:
        insert_loc = 0
        while insert_loc < len(books) and int(books[insert_loc]['id']) == insert_loc + 1:
            insert_loc += 1
        books.insert(insert_loc, {'title': request.form['name'], 'id': str(insert_loc + 1) })
        return redirect(url_for('showBook'))

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    if request.method == 'GET':
        return render_template('editBook.html', book_id=book_id)
    else:
        for book in books:
            if book['id'] == str(book_id):
                book['title'] = request.form['name']
        return redirect(url_for('showBook'))
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    book_title = None
    book_index = None
    for i, book in enumerate(books):
        if book['id'] == str(book_id):
            book_title = book['title']
            book_index = i

    if request.method == 'GET':
        return render_template('deleteBook.html', book_title=book_title, book_id=book_id)
    else:
        del books[book_index]
        return redirect(url_for('showBook'))

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

